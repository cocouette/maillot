<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet" />
    <link href="default.css" rel="stylesheet" type="text/css" media="all" />
    <link href="fonts.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/9f2ec18f92.js"></script>
    <!--[if IE 6]><link href="default_ie6.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>
<body>

<div id="header-wrapper">
	<div id="header" class="container">
		<div id="logo">
			<h1><a href="#">Handshirt</a></h1>
		</div>
		<div id="menu">
			<ul>
				<li class="active"><a href="index.php" title="">Homepage</a></li>
				<li><a href="views/flocage/index.php" title="">Flocage</a></li>
                <?php

                if ($_SESSION['userCo'] == 1){
                    ?>
                    <li><a href="views/admin/adminView.php"> Mon Compte </a></li>
                    <li><a href="views/admin/disconnect.php"><i class="fas fa-power-off"></i> Déconnexion </a></li>
                <?php } else {?>
                    <li><a href="views/admin/index.php"><i class="fas fa-sign-in-alt"></i> Connexion </a></li>
                <?php } ?>
			</ul>
		</div>
	</div>
    <?php include("views/layout/banner.php") ?>
</div>

<div id="wrapper">
	<div id="staff" class="container">
		<div class="title">
			<h2>Nos meilleures ventes</h2>
			<br />
			<br />
			<br />
		<div class="boxA"><img src="images/MHB.PNG" width="300" height="450" alt="" /></div>
		<div class="boxB"><img src="images/PSG.PNG" width="300" height="450" alt="" /></div>
		<div class="boxC"><img src="images/Nantes.PNG" width="300" height="450" alt="" /></div>
	</div>
	<div id="page" class="container">
		<div class="boxA">
			<h2>MONTPELLIER<br />
				<span>65,00 €</span></h2>
			<p>Donec leo, vivamus fermentum nibh in augue praesent a lacus at urna congue rutrum.</p>
			<ul class="style4">
				<li class="first"><a href="#">Consectetuer adipiscing elit</a></li>
				<li><a href="#">Metus aliquam pellentesque</a></li>
				<li><a href="#">Proin gravida orci porttitor</a></li>
			</ul>
		</div>
		<div class="boxB">
			<h2>PARIS SAINT GERMAIN<br />
				<span> 59,50 €</span></h2>
			<ul class="style3">
				<li class="first">
					<p class="date"><a href="#">Dec<b>30</b></a></p>
					<h3>Amet sed volutpat mauris</h3>
					<p><a href="#">Mauris tempus nibh sodales adipiscing dolore.</a></p>
				</li>
				<li>
					<p class="date"><a href="#">Dec<b>28</b></a></p>
					<h3>Sagittis diam dolor amet</h3>
					<p><a href="#">Duis arcu tortor fringilla sed  sed magna.</a></p>
				</li>
			</ul>
		</div>
		<div class="boxC">
			<h2>NANTES<br />
				<span>60,00 €</span></h2>
			<ul class="style4">
				<li class="first"><a href="#">Consectetuer adipiscing elit</a></li>
				<li><a href="#">Metus aliquam pellentesque</a></li>
				<li><a href="#">Suspendisse iaculis mauris</a></li>
				<li><a href="#">Urnanet non molestie semper</a></li>
				<li><a href="#">Suspendisse iaculis mauris</a></li>
			</ul>
		</div>
	</div>
</div>

<?php include("views/layout/footer.php"); ?>

