-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 04, 2019 at 01:21 PM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `Vente_Maillot`
--

-- --------------------------------------------------------

--
-- Table structure for table `choisit`
--

CREATE TABLE `choisit` (
  `Numero_Client` int(11) NOT NULL,
  `Numero_Maillot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `choisit`
--

INSERT INTO `choisit` (`Numero_Client`, `Numero_Maillot`) VALUES
(1, 3),
(1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `Client`
--

CREATE TABLE `Client` (
  `Numero_Client` int(11) NOT NULL,
  `Nom_Client` varchar(255) DEFAULT NULL,
  `Prenom_Client` varchar(255) DEFAULT NULL,
  `Adresse_Client` varchar(255) DEFAULT NULL,
  `Sexe_Client` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Client`
--

INSERT INTO `Client` (`Numero_Client`, `Nom_Client`, `Prenom_Client`, `Adresse_Client`, `Sexe_Client`) VALUES
(1, 'Le poulpe', 'Polo', '15 rue Louis Blériot 31000 Toulouse', 'F'),
(2, 'Dile', 'Croco', '68 avenue de l\'Amazone 31200 Toulouse', 'Non binaire'),
(3, 'Le déo', 'Nicole', '55 rue du sent bon', 'F'),
(8, 'BAR', 'Léni', '75 Avenue de la baleine 31000 Toulouse', 'F'),
(9, 'A la vanille please', 'Douglas', '55 Rue du cornet 31000 Toulouse', 'H'),
(10, 'Dubois', 'Jean', '45 rue de la forêt 31300 Toulouse', 'H');

-- --------------------------------------------------------

--
-- Table structure for table `commande_en_fonction`
--

CREATE TABLE `commande_en_fonction` (
  `Numero_Client` int(11) NOT NULL,
  `Numero_Stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `commande_en_fonction`
--

INSERT INTO `commande_en_fonction` (`Numero_Client`, `Numero_Stock`) VALUES
(3, 1),
(10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `definit`
--

CREATE TABLE `definit` (
  `Numero_Client` int(11) NOT NULL,
  `Numero_Flocage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `Equipe`
--

CREATE TABLE `Equipe` (
  `Club_Equipe` varchar(100) NOT NULL,
  `Joueur_Equipe` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Equipe`
--

INSERT INTO `Equipe` (`Club_Equipe`, `Joueur_Equipe`) VALUES
('Aix', 'Aymeric MINNE'),
('Chambéry', 'Fahrudin MELIC'),
('Dunkerque', 'Baptiste BUTTO'),
('Montpellier', 'Melvyn RICHARDSON'),
('Nantes', 'Valero RIVERA'),
('Nîmes', 'Mohammad SANAD'),
('Paris', 'Mikkel HANSEN'),
('Saint-Raphaël', 'Raphaël CAUCHETEUX'),
('Toulouse', 'Markus OLSSON'),
('Tremblay', 'Pedro PORTELA');

-- --------------------------------------------------------

--
-- Table structure for table `Flocage`
--

CREATE TABLE `Flocage` (
  `Numero_Flocage` int(11) NOT NULL,
  `Numero_joueur_Flocage` int(11) DEFAULT NULL,
  `Nom_joueur_Flocage` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Flocage`
--

INSERT INTO `Flocage` (`Numero_Flocage`, `Numero_joueur_Flocage`, `Nom_joueur_Flocage`) VALUES
(1, 44, 'Jean Michel '),
(2, 52, 'Nicolas Le Déaut');

-- --------------------------------------------------------

--
-- Table structure for table `Maillot`
--

CREATE TABLE `Maillot` (
  `Numero_Maillot` int(11) NOT NULL,
  `Taille_Maillot` varchar(255) DEFAULT NULL,
  `Prix_Maillot` float DEFAULT NULL,
  `Genre_Maillot` varchar(255) DEFAULT NULL,
  `flocage_numero_flocage` int(11) DEFAULT NULL,
  `equipe_club_equipe` varchar(100) DEFAULT NULL,
  `Numero_Stock` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Maillot`
--

INSERT INTO `Maillot` (`Numero_Maillot`, `Taille_Maillot`, `Prix_Maillot`, `Genre_Maillot`, `flocage_numero_flocage`, `equipe_club_equipe`, `Numero_Stock`) VALUES
(2, 'S', 50, 'F', 2, 'Aix', 222502),
(5, ' S', 100, '1', NULL, 'Paris', NULL),
(16, 'S', 12, '2', NULL, 'MHB', NULL),
(33, 'XXL', 66, '1', 33, 'Paris', 0),
(667, '', 0, '', NULL, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Stock`
--

CREATE TABLE `Stock` (
  `Numero_Stock` int(11) NOT NULL,
  `Taille_Stock` varchar(255) DEFAULT NULL,
  `Genre_Stock` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Stock`
--

INSERT INTO `Stock` (`Numero_Stock`, `Taille_Stock`, `Genre_Stock`) VALUES
(111501, '10', 'M'),
(222502, '10', 'F');

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `username`, `password`, `isAdmin`) VALUES
(10, 'admin', 'admin', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `choisit`
--
ALTER TABLE `choisit`
  ADD PRIMARY KEY (`Numero_Client`,`Numero_Maillot`),
  ADD KEY `FK_choisit_Numero_Maillot` (`Numero_Maillot`);

--
-- Indexes for table `Client`
--
ALTER TABLE `Client`
  ADD PRIMARY KEY (`Numero_Client`);

--
-- Indexes for table `commande_en_fonction`
--
ALTER TABLE `commande_en_fonction`
  ADD PRIMARY KEY (`Numero_Client`,`Numero_Stock`),
  ADD KEY `FK_commande_en_fonction_Numero_Stock` (`Numero_Stock`);

--
-- Indexes for table `definit`
--
ALTER TABLE `definit`
  ADD PRIMARY KEY (`Numero_Client`,`Numero_Flocage`),
  ADD KEY `FK_definit_Numero_Flocage` (`Numero_Flocage`);

--
-- Indexes for table `Equipe`
--
ALTER TABLE `Equipe`
  ADD PRIMARY KEY (`Club_Equipe`);

--
-- Indexes for table `Flocage`
--
ALTER TABLE `Flocage`
  ADD PRIMARY KEY (`Numero_Flocage`);

--
-- Indexes for table `Maillot`
--
ALTER TABLE `Maillot`
  ADD PRIMARY KEY (`Numero_Maillot`),
  ADD KEY `FK_Maillot_flocage_numero_flocage` (`flocage_numero_flocage`),
  ADD KEY `FK_Maillot_equipe_club_equipe` (`equipe_club_equipe`),
  ADD KEY `FK_Maillot_Numero_Stock` (`Numero_Stock`);

--
-- Indexes for table `Stock`
--
ALTER TABLE `Stock`
  ADD PRIMARY KEY (`Numero_Stock`);

--
-- Indexes for table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Client`
--
ALTER TABLE `Client`
  MODIFY `Numero_Client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `Flocage`
--
ALTER TABLE `Flocage`
  MODIFY `Numero_Flocage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Maillot`
--
ALTER TABLE `Maillot`
  MODIFY `Numero_Maillot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=668;

--
-- AUTO_INCREMENT for table `Stock`
--
ALTER TABLE `Stock`
  MODIFY `Numero_Stock` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222503;

--
-- AUTO_INCREMENT for table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

