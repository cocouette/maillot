<?php

require "../../config.php";

$connex =  new PDO( 'mysql:host='.DB_HOST.';dbname='.DB_DATABASE, DB_USERNAME, DB_PASSWORD );

//Si cette connexion échoue, on quitte le script (die)
if ( ! $connex ) die( "Impossible de se connecter à MySQL" );

$username = $_POST['username'];
$password = $_POST['password'];

$_SESSION = array();

$usernames=array();
$passwords=array();
$isAdmins=array();

$req="SELECT * from utilisateur WHERE username=:username and password=:password";
$res=$connex ->prepare ($req);
$res->execute( array (
    'username'=> $username,
    'password'=> $password,
    'isAdmin'=> $isAdmin,
));

while ($row=$res -> fetch(PDO::FETCH_OBJ)) {
    $usernames[] = $row->username;
    $passwords[] = $row->password;
    $isAdmins[] = $row->isAdmin;
}

$res->closeCursor();

if(!isset($usernames[0]) AND !empty($usernames[0])) {
    $_SESSION['username'] = $usernames[0];

    if ($_SESSION['username'] == 'admin') {
        $GLOBALS['isAdmin'] = 1;
    }
}
/*
if ($isAdmin == 1){
    header('Location: adminView.php');
}
else {
    header('Location: index.php');
}*/

?>
