<?php
session_start();
?>
<?php include("../layout/default.php");?>
<div id="header-wrapper">
    <div id="header" class="container">
        <div id="logo">
            <h1><a href="#">Handshirt</a></h1>
        </div>
        <div id="menu">
            <ul>
                <li><a href="../../index.php">Homepage</a></li>
                <li><a href="../flocage/index.php">Flocage</a></li>
                <?php include("../layout/navbar.php") ?>
            </ul>
        </div>
    </div>
    <div id="banner" class="container"><span>DANS LA PEAU DES PLUS<br /> GRANDS JOUEURS</span></div>
</div>

<?php echo $isAdmin; ?>

<?php if ($_SESSION['userCo']==1) { ?>

    <div id="wrapper">
        <div id="staff" class="container">
            <div class="title">
                <h2>Mon espace de gestion</h2>
            </div>
            <div id="page" class="container">
                <div id="row">
                    <div class="col">
                        <h2>Créer un nouveau maillot</h2>
                        <form action="Insert.php" method="GET"> <!--remplacez le #1 par la page cible où vous effectuer le traitement du formulaire -->

                            Choix equipe <select name="equipe"><option value ='Paris'>Paris</option><br><option value ='Montpellier'>Montpellier</option><br><option value ='Nantes'>Nantes</option><br><option value ='Dunkerque'>Dunkerque</option><br><option value ='Chambéry'>Chambéry</option><br><option value ='Toulouse'>Toulouse</option><br><option value ='Nîmes'>Nîmes</option><br><option value ='Tremblay'>Tremblay</option><br><option value ='Saint-Raphaël'>Saint-Raphaël</option><br><option value ='Aix'>Aix</option><br></select>
                            <br> <!-- balise de retour à la ligne -->
                            <br> <!-- balise de retour à la ligne -->

                            Numero maillot <input name="numero" type="text"> <!--remplacez le #1 par le nom de la variable de ce champ texte-->
                            <br> <!-- balise de retour à la ligne -->
                            <br> <!-- balise de retour à la ligne -->

                            <!-- pour avoir un même groupe de radio boutons mettez le même name à la place de #1 et remplacez les textes affichés choix 1 ... choix 4 et les valeurs associées #val1 ... #val4 -->
                            Homme <input name="genre" type="radio" value="1">Femme <input name="genre" type="radio" value="2">
                            <br> <!-- balise de retour à la ligne -->
                            <br> <!-- balise de retour à la ligne -->
                            Taille
                            <select name="taille">
                                <option value ='XS'>XS</option>
                                <br>
                                <option value ='S'>S</option>
                                <br>
                                <option value ='M'>M</option>
                                <br>
                                <option value ='L'>L</option>
                                <br>
                                <option value ='XL'>XL</option>
                                <br>
                                <option value ='XXL'>XXL</option>
                                <br>
                            </select>
                            <br> <!-- balise de retour à la ligne -->
                            <br> <!-- balise de retour à la ligne -->

                            Prix <input name="prix" type="text"> <!--remplacez le #1 par le nom de la variable de ce champ texte-->
                            <br> <!-- balise de retour à la ligne -->
                            <br> <!-- balise de retour à la ligne -->

                            Numéro de flocage <input name="numero_flocage" type="text">
                            <br> <!-- balise de retour à la ligne -->
                            <br> <!-- balise de retour à la ligne -->

                            Référence <input name="référence" type="text">
                            <br> <!-- balise de retour à la ligne -->
                            <br> <!-- balise de retour à la ligne -->


                            <!-- bouton envoyer (submit) bouton de validation d'un formulaire-->
                            <input type="submit" value="Envoyer">
                        </form> <!-- fin de formulaire -->
                    </div>
                </div>

                <!-- DELETE A NEW -->
                <div class="col">
                    <h2>Supprimer un maillot :</h2>
                    <form action="delete.php" method="post">
                        <p>
                            <label>Supprimer un maillot dont la référence est :</label>
                            <input type="number" name="Numero_Stock" />
                            <br>

                            <input type="submit" value="Envoyer" />
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } else {?>
    <div class="text-center">
        <h2>Vous n'avez pas accès à cette page.</h2>
        <a href="index.php">Veuillez vous connectez</a>
    </div>
    <br>
    <br>
    <br>
<?php } ?>

<?php include("../layout/footer.php") ?>

