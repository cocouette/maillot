<html>
<head>
<title>Liste Articles</title>
</head>
<body>
<h1>Liste Articles</h1>
<br>
<?php
//Accès au fichier de conf bdd sur le dossier parent
require "../../config.php";

//Connexion au serveur de BDD se trouvant sur la machine dev2.icam.fr en 'localhost'
$link = mysqli_connect( "localhost", DB_USERNAME, DB_PASSWORD, DB_DATABASE );

//Si cette connexion échoue, on quitte le script (die)
if ( ! $link ) die( "Impossible de se connecter à MySQL" );

//exécuter la requête et récupérer le résultat dans la variable $result
$result = mysqli_query( $link, "SELECT Numero_Maillot,Taille_Maillot,Prix_Maillot,Genre_Maillot,flocage_numero_flocage,equipe_club_equipe,Numero_Stock FROM Maillot" );
//récupérer ligne à ligne le contenu de $result dans $uneLigne avec la fonction mysql_fetch_assoc(...)
?>
<table width="50%" border="1" align="center"> <!-- ce tableau prend 50% de la page, a une bordure de 1 pixel et est centré -->
<?php  
while ( $uneLigne = mysqli_fetch_assoc( $result ) )
{
   print("<tr>");
       print ("<td>Le numéro de maillot est : ".$uneLigne['Numero_Maillot']."<br></td>");//affichage du numéro de maillot
	   print ("<td>La taille du maillot est : ".$uneLigne['Taille_Maillot']."<br></td>"); //affichage de la taille du maillot
	   print ("<td>Le prix du maillot est de : ".$uneLigne['Prix_Maillot']."<br></td>"); //affichage du prix du maillot
	   print ("<td>Le genre du maillot est : ".$uneLigne['Genre_Maillot']."<br></td>"); //affichage du genre du maillot
	   print ("<td>Le club du maillot est : ".$uneLigne['equipe_club_equipe']."<br></td>"); //affichage du club du maillot
	   print ("<td>Le numéro du flocage de l'équipe est : ".$uneLigne['flocage_numero_flocage']."<br></td>"); //affichage du numéro de flocage du maillot
	   print ("<td>La référence du maillot est : ".$uneLigne['Numero_Stock']."<br></td>"); //affichage de la référence du maillot
    print("</tr>");
}
?>
</table>



<!-- tableau HTML (balises <table> et </table>) à 3 lignes (balises <tr> et </tr> 2 colonnes par lignes (balises <td> et </td> --> 


</body>
</html>
