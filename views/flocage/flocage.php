<?php

require "../../config.php";

$connex =  new PDO( 'mysql:host='.DB_HOST.';dbname='.DB_DATABASE, DB_USERNAME, DB_PASSWORD );

//Si cette connexion échoue, on quitte le script (die)
if ( ! $connex ) die( "Impossible de se connecter à MySQL" );

$numMaillot = $_POST['Numero_Maillot'];
$prixMaillot = $_POST['Prix_Maillot'];
$tailleMaillot = $_POST['taille_Maillot'];
$genreMaillot = $_POST['Genre_Maillot'];
$flocageMaillot = $_POST['flocage_numero_flocage'];
$equipeMaillot = $_POST['equipe_club_equipe'];

$numMaillot=array();
$prixMaillot=array();
$tailleMaillot=array();
$genreMaillot=array();
$flocageMaillot=array();
$equipeMaillot=array();


$req="SELECT * from Maillot";

$res=$connex ->prepare ($req);

$res->execute( array (
    'Numero_Maillot'=> $numMaillot,
    'Prix_Maillot'=> $prixMaillot,
    'taille_Maillot'=> $tailleMaillot,
    'Genre_Maillot'=> $genreMaillot,
    'flocage_numero_flocage'=> $flocageMaillot,
    'equipe_club_equipe'=> $equipeMaillot,
));

while ($row=$res -> fetch(PDO::FETCH_OBJ)) {
    $numMaillot[] = $row->Numero_Maillot;
    $prixMaillot[] = $row->Prix_Maillot;
    $tailleMaillot[] = $row->taille_Maillot;
    $genreMaillot[] = $row->Genre_Maillot;
    $flocageMaillot[] = $row->flocage_numero_flocage;
    $equipeMaillot[] = $row->equipe_club_equipe;
}

$res->closeCursor();

?>
