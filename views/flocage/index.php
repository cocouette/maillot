<?php
session_start();
?>
<?php include("../layout/default.php") ?>
<div id="header-wrapper">
	<div id="header" class="container">
		<div id="logo">
			<h1><a href="#">Handshirt</a></h1>
		</div>
		<div id="menu">
			<ul>
				<li><a href="../../index.php">Homepage</a></li>
				<li class="active"><a href="index.php">Flocage</a></li>
                <?php include("../layout/navbar.php") ?>
			</ul>
		</div>
	</div>
    <?php include("../layout/banner.php") ?>
</div>

<div class="blog-area section">
    <div class="container">
        <div class="row">

            <?php
            $i=0;
            if($res!=false){
            foreach($res as &$u){?>

            <div class="col-lg-4 col-md-6">
                <div class="card h-100">
                    <div class="single-post post-style-1">
                        <div class="blog-image"><img src="<?php echo $confs[$i]['photo']?>" alt="Blog Image"></div>
                        <a class="avatar" href="<?= SERVER_WWW?>/conf-project/users/view/<?= $organisateur[$i][0]["id"]?>"><img src="<?php echo $organisateur[$i][0]['avatar'];?>" alt="Profile Image"></a>
                        <div class="blog-info">
                            <h4 class="title"><b><a href="<?= SERVER_WWW?>/conf-project/conferences/view/<?= $confs[$i]['id']?>"><?php echo $confs[$i]['titre']; ?></a></b></h4>

                            <ul class="post-footer">
                                <li><a href="#"><i class="ion-heart"></i><?php echo $u['jaime']; ?></a></li>
                                <li><a href="#"><i class="ion-chatbubble"></i><?php echo $coms[$i][0]["count(lien_conference)"]; ?></a></li>
                                <li><a href="#"><i class="ion-eye"></i><?php echo $u['nb_participants']; ?></a></li>
                            </ul>
                        </div><!-- blog-info -->
                    </div><!-- single-post -->
                </div><!-- card -->
            </div><!-- col-lg-4 col-md-6 -->

                <?php
                $i++;
                }
            }
            else{
                echo "<a class=\"text-center\" href=\"../admin/adminView.php\"><h4>Il n'y a pas de maillots, créer votre maillot! </a></h4>";
            }
            ?>


        </div>
    </div>

</div>


<!-- CODE PRÉSENT DE BASE DANS LE TEMPLATE
<div id="wrapper">
	<div id="staff" class="container">
		<div class="title">
			<h2>Nos meilleures ventes</h2>
            <br />
			<br />
			<br />
		<div class="boxA"><img src="../../images/MHB.PNG" width="300" height="450" alt="" /></div>
		<div class="boxB"><img src="../../images/PSG.PNG" width="300" height="450" alt="" /></div>
		<div class="boxC"><img src="../../images/Nantes.PNG" width="300" height="450" alt="" /></div>
	</div>
	<div id="page" class="container">
            <div class="boxA">
                <h2>Une équipe de foot de ouf</h2>
                <p>Donec leo, vivamus fermentum nibh in augue praesent a lacus at urna congue rutrum.</p>
                <ul class="style4">
                    <li class="first"><a href="#">Consectetuer adipiscing elit</a></li>
                    <li><a href="#">Metus aliquam pellentesque</a></li>
                    <li><a href="#">Proin gravida orci porttitor</a></li>
                </ul>
            </div>
		<div class="boxB">
			<h2>PARIS SAINT GERMAIN<br />
				<span> 59,50 €</span></h2>
			<ul class="style3">
				<li class="first">
					<p class="date"><a href="#">Dec<b>30</b></a></p>
					<h3>Amet sed volutpat mauris</h3>
					<p><a href="#">Mauris tempus nibh sodales adipiscing dolore.</a></p>
				</li>
				<li>
					<p class="date"><a href="#">Dec<b>28</b></a></p>
					<h3>Sagittis diam dolor amet</h3>
					<p><a href="#">Duis arcu tortor fringilla sed  sed magna.</a></p>
				</li>
			</ul>
		</div>
		<div class="boxC">
			<h2>NANTES<br />
				<span>60,00 €</span></h2>
			<ul class="style4">
				<li class="first"><a href="#">Consectetuer adipiscing elit</a></li>
				<li><a href="#">Metus aliquam pellentesque</a></li>
				<li><a href="#">Suspendisse iaculis mauris</a></li>
				<li><a href="#">Urnanet non molestie semper</a></li>
				<li><a href="#">Suspendisse iaculis mauris</a></li>
			</ul>
		</div>
	</div>
        <div id="welcome" class="container">
            <div class="title">
                <h2>Welcome to our website</h2>
            </div>
            <p>This is <strong>RedMarket</strong>, a free, fully standards-compliant CSS template designed by <a href="http://templated.co" rel="nofollow">TEMPLATED</a>. The photos in this template are from <a href="http://fotogrph.com/"> Fotogrph</a>. This free template is released under the <a href="http://templated.co/license">Creative Commons Attribution</a> license, so you're pretty much free to do whatever you want with it (even use it commercially) provided you give us credit for it. Have fun :) </p>
        </div>
    </div>
</div>-->

<?php include("../layout/footer.php") ?>
